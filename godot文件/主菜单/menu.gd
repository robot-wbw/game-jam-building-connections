extends Control

@onready var ui:AudioStreamPlayer = $UI
@onready var help_page:Control = %HelpPage
@onready var credit_page:Control = %CreditPage

## 主菜单，直接提供3个关卡的跳转？不，还是让它们从头开始玩吧？（沉浸感好些）
#有个小trick：主菜单背景音乐偏小，便促使玩家把声音放大，游戏bgm代入感就上来了（）


func _on_start_pressed():
	get_tree().change_scene_to_file("res://关卡/实装关卡/关卡1.tscn")
	


func _on_help_pressed():
	help_page.show()


func _on_credit_pressed():
	credit_page.show()


func _on_exit_pressed():
	if OS.get_name() != "Web":
		get_tree().quit()

# =====================UI发出声音===========================
func _on_start_mouse_entered():
	ui.play()
	pass

func _on_help_mouse_entered():
	ui.play()
	pass

func _on_credit_mouse_entered():
	ui.play()
	pass

func _on_exit_mouse_entered():
	ui.play()
	pass
