extends Node3D
class_name Unit


## Gridmap上有功能的单元格需要额外生成此节点来实现功能
## 而生成方法并不在此类中，需要外部（如main或Gridmap的代码）初始化时进行生成
## Unit可以包括资源点甚至可能地面，因此不分阵营
@export var hight:float = 0.5 #模型的大致高度，healthbar之类的可以利用这个参数进行调整
var parent_map:WorldMap ## 与对应的Gridmap绑定（注：Gridmap保证position在0,0,0，中途不要移动）
var map_position:Vector3i ## 在gridmap中的位置


##因为现在使用Gridmap直接管理，所以可以直接指定上级为gridmap了
func _ready():
	parent_map = get_parent_node_3d()
	map_position = parent_map.local_to_map(position) # 确定自己在地图的位置

## 两种方法，一种是被击杀，一种是建筑替换（？）
## 不过实际上可以利用tree_exiting来调用……除非中间有个“濒死状态”

	

