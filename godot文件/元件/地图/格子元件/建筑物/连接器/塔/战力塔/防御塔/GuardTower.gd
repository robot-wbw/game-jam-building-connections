extends WarTower
class_name GuardTower

#有子节点（碰撞箱），必须得用场景
var shooter:Shooter = preload("res://元件/战斗系统/枪手/Shooter.tscn").instantiate()

func _ready():
	super()
	target_body.health = 150
	target_body.max_health = 150
	
	add_child(shooter)
	shooter.position.y = 1
	shooter.hunt_range = 10
	
func set_active(choice:bool = false):
	super(choice)
	shooter.is_active = choice
	
func set_team(team:int = 1):
	super(team) #队伍一般在游戏中不会变，但是可以备用……另外注意super要传参数
	shooter.team = team
