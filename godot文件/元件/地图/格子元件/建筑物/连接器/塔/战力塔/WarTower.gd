extends Tower
class_name WarTower
## 主动防御，可以自己有操作，而不必像资源塔那样被集中管理

## 冷却对于不同的塔有不同意义：防御塔必须要等待敌人接近，而兵力塔可以不管周围是否有敌军，不停地生产
#@export var cooldown:float = 1
#好吧还是各自找自己的冷却方法吧

func _ready():
	hight=1.0
	super()
