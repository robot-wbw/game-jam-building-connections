extends Building

class_name Connectable

## 连接其他连接器以保持活力
#虽说目前基本全部Building都是connector，但是如果是要地雷之类，不需要连接但是属于“建筑”，那就另外要有类了

@export var independent := false #敌方塔以及己方主塔塔不需要其他连接就能存活

# 通过独立连接器扩散，寻找其他己方连接器
func _ready():
	super()
	
