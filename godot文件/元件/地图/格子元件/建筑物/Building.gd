extends Unit

class_name Building
## 人为建筑（或地图预设），有生命值，有阵营

#var health_bar:Label3D = preload("res://元件/地图/子元件/health_bar.tscn").instantiate()

## 直接给出子节点位置，如果有什么需要更改的，通过这个更改就好

#生命值集成在target_body上，有什么事情直接找它
var target_body:TargetBody = preload("res://元件/战斗系统/承伤单位/TargetBody.tscn").instantiate()
signal die(place:Vector3i)
## 阵营
## 目前规定1属于玩家，2属于敌军
## 从bool改为int是考虑日后可以混战，也可以多玩家对抗
## 不同阵营目前大概可以以连接线颜色、光亮颜色区分？
@export var team := 1 :
	set(x):
		team = x
		set_team(x)
func set_team(team:int = 1):
	#用于后面继承的类来重写
	target_body.team=team
	set_halo_color()
	
	
var halo:SpotLight3D = preload("res://元件/地图/子元件/deafault_halo.tscn").instantiate()
@export var halo_color:Color = Color.RED:
	set(color): #完全转移，自己永远纯白，但以灯实际颜色为伪装（）
		halo.light_color=color
		halo_color =color
	#get:
		#return halo.light_color

@export var halo_energy: = halo.light_energy:
	set(energy): #完全转移
		halo.light_energy=energy
	get:
		return halo.light_energy

## 是否活跃中
## 如果属于友方且与主树失去连接，将会失活
## 如果在游戏中更改状态，请使用activate(choice)函数
@export var is_activated:bool = false :
	set(x):
		is_activated=x
		set_active(x)
func set_active(choice:bool = false):
	# set时调用，方便后面重写函数
	set_halo_color()

## 二合一，choice为true则重新活跃，false为失活
## 如果设定状态与当前状态相同则会失效
## 设置此函数是方便做动画之类……不过这里只是多发送信号吧
func set_halo_color():
	if team ==1:
		if is_activated == true :
			halo_color = Color.WHITE
		else:
			halo_color=Color.RED
	if team ==2:
		halo_color = Color.PURPLE
	else:
		pass


func _ready():
	super()
	#health_bar.position.y += hight
	#add_child(health_bar)
	die.connect(parent_map._on_building_die)
	add_child(target_body)
	target_body.die.connect(go_die)
	
	
	set_halo_color()
	add_child(halo)

## 继承属性……不过现在打算先不继承属性了
#func inherit(predecessor:Building):
	#health = predecessor.health
	#team = predecessor.team
	#is_activated = predecessor.is_activated
func go_die():
	die.emit(map_position)
	queue_free()
