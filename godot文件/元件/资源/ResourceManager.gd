extends Node

## 游戏中的资源一共三样：叶、水、种子[br]
## 获取资源通过采集塔———可以隔一段时间统计采集塔数量
## 建造需要消耗资源，而如果资源不够则不允许建造
## 此外注意，资源储量是有上限的，但多一些种子塔，三种资源的存量上限都会提高

@onready var world_map:WorldMap = %WorldMap

@onready var resource_timer:Timer = $ResourceTimer


var leaf_stock:float = 20:
	set(x):
		leaf_stock=x
		update_leaf.emit(x)
var water_stock:float = 20:
	set(x):
		water_stock=x
		update_water.emit(x)
var seed_stock:float = 2:
	set(x):
		seed_stock = x
		update_seed.emit(x)

#最大值也要改改
signal update_leaf(value:float)
signal update_water(value:float)
signal update_seed(value:float)

var leaf_stock_max:float = AMOUONT_UPGRADE[0]*3:
	set(x):
		leaf_stock_max=x
		update_leaf_max.emit(x)
var water_stock_max:float = AMOUONT_UPGRADE[1]*3:
	set(x):
		water_stock_max=x
		update_water_max.emit(x)
var seed_stock_max:float = AMOUONT_UPGRADE[2]*3:
	set(x):
		seed_stock_max = x
		update_seed_max.emit(x)

#已经在变量的setter里了，更新数据会自动发送信号
signal update_leaf_max(value:float)
signal update_water_max(value:float)
signal update_seed_max(value:float)

const AMOUONT_UPGRADE:Array[int]= [10,10,1]
const RESOURCE_SPEED:Array[float]= [0.02,0.02,0.005]

# 资源管理，交给ResourceManager

#信号能正常传输Array
#字典自己的问题：本身出来的时候Array都是Variant，单独拆开倒是可以用
signal cost_reply(id:WorldMap.ID,costs:Array[int])
signal build_fail
signal build(place:Vector3i,id:WorldMap.ID)


var CostList:Dictionary = { #目前并不涨价，但是日后可以考虑涨价
	WorldMap.ID.CONNECTOR : [1,1,0],
	WorldMap.ID.LEAF_COLLECTOR : [5,5,1],
	WorldMap.ID.WATER_COLLECTOR : [5,5,1],
	WorldMap.ID.SEED_COLLECTOR : [15,15,2],
	WorldMap.ID.GUARD_TOWER : [4,4,1],
	WorldMap.ID.ARMY_TOWER : [5,5,0], 
}

func _ready():
	#地图现在是动态的，用代码连接一下
	build.connect(world_map.setblock)
	
	update_ui()

func update_ui():
	update_leaf_max.emit(leaf_stock_max)
	update_leaf.emit(leaf_stock)
	update_water_max.emit(water_stock_max)
	update_water.emit(water_stock)
	update_seed_max.emit(seed_stock_max)
	update_seed.emit(seed_stock)




func _on_ui_cost_how_much(id):
	var costs:Array[int] = [CostList[id][0],CostList[id][1],CostList[id][2]]
	#下面这段代码会报错，而上面的正常（估计Array自己没有类型转换）
	#var costs:Array[int] = CostList[id] 
	#PackedInt64Array……目前打模，先不用吧
	cost_reply.emit(id,costs)


func _on_player_build(place:Vector3i,id:WorldMap.ID):
	if CostList.has(id):
		var costs:Array[int] = [CostList[id][0],CostList[id][1],CostList[id][2]]
		var remain:Array[float] = [leaf_stock-costs[0],water_stock-costs[1],seed_stock-costs[2]]
		for i in remain:
			if i < 0:
				#材料不足，立刻终止操作
				build_fail.emit()
				return
		#如果成功，继续计算
		build.emit(place,id)
		leaf_stock = remain[0]
		water_stock = remain[1]
		seed_stock = remain[2]
		#特殊：如果建造的是种子塔，提升资源上限
		if id ==WorldMap.ID.SEED_COLLECTOR:
			leaf_stock_max += AMOUONT_UPGRADE[0]
			water_stock_max += AMOUONT_UPGRADE[1]
			seed_stock_max += AMOUONT_UPGRADE[2]
	


# 定时获取资源，并考虑上限问题
func resource_routine():
	# 叶
	var choose_leaf_collector:Callable = func(x:Unit)->bool:
		if x is LeafCollector:
			return x.is_activated
		return false
	leaf_stock += world_map.count_units(choose_leaf_collector)*RESOURCE_SPEED[0]
	leaf_stock = clampf(leaf_stock,0,leaf_stock_max)
	# 水
	var choose_water_collector:Callable = func(x:Unit)->bool:
		if x is WaterCollector:
			return x.is_activated
		return false
	water_stock += world_map.count_units(choose_water_collector)*RESOURCE_SPEED[1]
	water_stock = clampf(water_stock,0,water_stock_max)
	# 种子
	var choose_seed_collector:Callable = func(x:Unit)->bool:
		if x is SeedCollector:
			return x.is_activated
		return false
	seed_stock += world_map.count_units(choose_seed_collector)*RESOURCE_SPEED[2]
	seed_stock = clampf(seed_stock,0,seed_stock_max)
