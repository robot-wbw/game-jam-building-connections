extends CenterContainer
@onready var animation_player = %AnimationPlayer
@onready var label = %Label

func show_message(message:String,is_longer:bool=false,text_color:Color=Color.WHITE):
	label.text = message
	label.modulate = text_color
	animation_player.stop()
	if is_longer:
		animation_player.play("fade_out_longer")
	else:
		animation_player.play("fade_out")
