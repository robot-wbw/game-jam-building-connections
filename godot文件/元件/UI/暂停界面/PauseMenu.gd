extends Control


func _on_continue_pressed():
	get_tree().paused = false
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	hide()
	

func _on_exit_pressed():
	get_tree().paused = false
	get_tree().change_scene_to_file("res://主菜单/menu.tscn")
	
func _on_restart_pressed():
	get_tree().paused = false
	get_tree().reload_current_scene()
