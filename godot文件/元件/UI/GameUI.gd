extends Control


#@onready var resource_manager:Node = %ResourceManager #利用信号异步代码，这里不需要了


## UI现在既显示资源，也管理资源

@onready var leaf_bar:Control = %LeafBar
@onready var water_bar:Control = %WaterBar
@onready var seed_bar:Control = %SeedBar
@onready var pause_menu:Control = %PauseMenu
@onready var building_plan:Control = $BuildingPlan

signal show_how_to_build(icon:Texture,name_tag:String,cost:Array[int])
signal cost_how_much(id:WorldMap.ID)
signal show_message(message:String)

signal update_leaf(value:float)
signal update_leaf_max(value:float)
signal update_water(value:float)
signal update_water_max(value:float)
signal update_seed(value:float)
signal update_seed_max(value:float)

# 名字与图片的字典交给UI
const icon_list:Dictionary ={
	WorldMap.ID.CONNECTOR : preload("res://素材/图像/各塔图片/01-线.png"),
	WorldMap.ID.LEAF_COLLECTOR : preload("res://素材/图像/各塔图片/05-采集塔-叶.png"),
	WorldMap.ID.WATER_COLLECTOR :preload("res://素材/图像/各塔图片/06-采集塔-水.png"),
	WorldMap.ID.SEED_COLLECTOR : preload("res://素材/图像/各塔图片/07-采集塔-种子.png"),
	WorldMap.ID.GUARD_TOWER : preload("res://素材/图像/各塔图片/08-防御塔.png"),
	WorldMap.ID.ARMY_TOWER : preload("res://素材/图像/各塔图片/10-兵力塔.png"), 
}
	
const name_list:Dictionary ={
	#WorldMap.ID.GROUND,
	WorldMap.ID.CONNECTOR : "连接器\nConnector",
	#WorldMap.ID.LEAF_RESOURCE : "Leaf Resource",
	#WorldMap.ID.WATER_RESOURCE : "Water Resource",
	#WorldMap.ID.SEED_RESOURCE : "Seed Resource",
	WorldMap.ID.LEAF_COLLECTOR : "叶绿采集器\nLeaf Collector",
	WorldMap.ID.WATER_COLLECTOR : "清水采集器\nWater Collector",
	WorldMap.ID.SEED_COLLECTOR : "种子采集器\nSeed Collector",
	WorldMap.ID.GUARD_TOWER : "防御塔\nGuard Tower",
	#WorldMap.ID.ENEMY_GUARD_TOWER :" Guard Tower",
	WorldMap.ID.ARMY_TOWER : "兵营塔\nArmy Tower",
	#WorldMap.ID.ENEMY_ARMY_TOWER :" Army Tower",
}




func _unhandled_input(event):
	if event.is_action_pressed("hide"): #F1隐藏界面
		visible = !visible


func _on_pause_pressed():
	get_tree().paused = true
	pause_menu.show()
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


func _on_player_how_to_build(id):
	if id != WorldMap.ID.NULL:
		building_plan.show()
		cost_how_much.emit(id)
	else:
		building_plan.hide()
	
func _on_resource_manager_cost_reply(id:WorldMap.ID,cost_reply:Array[int]):
	#第一次玩这种异步……原来是这边await，并且来回共两个信号两个接收函数
	show_how_to_build.emit(icon_list[id],name_list[id],cost_reply)
	
func _on_resource_manager_build_fail():
	show_message.emit("你没有足够的材料建造此建筑！\nYou don't have enough material for this!")



func _on_resource_manager_update_leaf(value:float):
	#毁灭吧，不想这么细地用信号了
	#好吧看来还是得写
	#看来至少ResourceBar那边的代码还是可以复用，一个value一个max
	update_leaf.emit(value)


func _on_resource_manager_update_water(value:float):
	update_water.emit(value)
	
func _on_resource_manager_update_seed(value:float):
	update_seed.emit(value)


func _on_resource_manager_update_leaf_max(value:float):
	update_leaf_max.emit(value)

func _on_resource_manager_update_water_max(value:float):
	update_water_max.emit(value)

func _on_resource_manager_update_seed_max(value:float):
	update_seed_max.emit(value)




func _on_world_map_win():
	pass # Replace with function body.


func _on_level_manager_show_message(the_text:String):
	show_message.emit(the_text,true)
