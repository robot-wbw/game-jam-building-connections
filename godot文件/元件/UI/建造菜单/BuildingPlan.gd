extends Control

## 3种资源
@onready var buiding_icon:TextureRect = %BuidingIcon
@onready var structure_name:Label = %StructureName
@onready var leaf_cost:Label = %LeafCost
@onready var water_cost:Label = %WaterCost
@onready var seed_cost:Label = %SeedCost


#
#class Structure:
	#var icon:Texture2D
	#var name_tag:String
	#var leaf_cost:int
	#var water_cost:int
	#var seed_cost:int
	#func Structure(icon:Texture2D,name_tag:String,leaf_cost:int,water_cost:int,seed_cost:int):
		#pass

func _on_game_ui_show_how_to_build(icon:Texture,name_tag:String,cost:Array[int]):
	buiding_icon.texture = icon
	structure_name.text = name_tag
	leaf_cost.text = String.num_int64(cost[0])
	water_cost.text = String.num_int64(cost[1])
	seed_cost.text = String.num_int64(cost[2])


