extends Control


#@onready var label = %Label #显示整数部分
#@onready var progress_bar = %ProgressBar #显示小数部分

@export var max_value:int = 30:
	set(x):
		max_value = x
		update_text.emit("%d / %d" % [int(value),x])
@export var value:float = 6 : #TODO:有个奇怪的一点：一开始显示0？
	set(x):
		value = x
		update_text.emit("%d / %d" % [int(x),max_value])
		update_progress.emit(x - int(x))

signal update_text(the_text:String)
signal update_progress(the_value:float)

func update_value(the_value):
	value = the_value
func update_max(the_value):
	max_value = the_value

