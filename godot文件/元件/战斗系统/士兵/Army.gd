extends Node3D
class_name Army
#实际上是受击与攻击的结合体，此外还有寻路！

@onready var target_body:TargetBody = $TargetBody
@onready var shooter:Shooter = $Shooter
@onready var navigation:NavigationAgent3D = $Navigation

@export var health:float = 50
@export var attack_range:float = 5
@export var team:int= 1


func _ready():
	navigation.target_position = Vector3(0,0,0)
	navigation.get_current_navigation_path()


func go_die():
	queue_free()
