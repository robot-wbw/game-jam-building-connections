extends Node3D
class_name ArmySpawner
## 用于定时生成士兵（冷却），也可控制敌方出军延迟

const army_resource:Resource = preload("res://元件/战斗系统/士兵/Army.tscn")
@onready var spawn_delay:Timer = $SpawnDelay #“敌军还有30秒到达战场”
@onready var cooldown:Timer = $Cooldown #“全军出击”

@export var delay_time:float = 1:
	set(x):
		if x<=0:
			delay_time = 1 #不能太小
		else:
			delay_time = x
@export var cooldown_time:float = 3:
	set(x):
		if x<=0:
			delay_time = 1 #不能太小
		else:
			delay_time = x
@export var team:int = 1


func _ready():
	spawn_delay.wait_time = delay_time
	cooldown.wait_time = cooldown_time

func _on_spawn_delay_timeout():
	cooldown.start()


func _on_cooldown_timeout():
	var new_army:Army = army_resource.instantiate()
	get_tree().add_child(new_army)
	new_army.global_position = global_position
	new_army.team = team
	#然后士兵自己寻路
