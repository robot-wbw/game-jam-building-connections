extends Node3D
class_name Shooter
#寻找TargetBody，以一定周期，对选定的一个进行射击

#需要有形状，只能如此
const bullet_resource:Resource = preload("res://元件/战斗系统/枪手/Bullet.tscn")

@onready var cooldown:Timer = $Cooldown
@export var cooldown_time:float = 0.5
#攻击范围
@onready var collision_shape_3d = $HuntingArea/CollisionShape3D
@export var hunt_range:float = 10 
@export var strength:float = 10

#队伍……
@export var team:int =1
@export var is_active:bool = false:
	set(x):
		is_active=x
		if is_node_ready(): #好吧好吧有这个就不怕了
			update()
var target_list:Array[TargetBody]=[]
var alarming:bool = false

#我感觉……寻找目标可能不是它的事？或者用Area?
# 碰撞现在用的第3层：猎人——受害者层
#可惜不区分队伍……

func _ready():
	var the_shape:SphereShape3D = SphereShape3D.new() #还必须新建一个，不然共享范围了
	collision_shape_3d.shape = the_shape
	the_shape.radius = hunt_range
	cooldown.wait_time = cooldown_time
	update()
	

func _on_prey_entered(prey:Area3D):
	if prey is TargetBody:
		if prey.team != team:
			#print("发现敌军！")
			#加入仇恨列表？然后按照仇恨列表依次攻击，直到所有节点都出去了
			target_list.append(prey)
			alarming = true
			update()

func _on_prey_exited(prey:Area3D):
	if target_list.has(prey):
		target_list.remove_at(target_list.find(prey))
	if target_list.is_empty():
		alarming = false
		update()

func update():
	if alarming and is_active:
		if cooldown.is_stopped():
				cooldown.start()
	else:
		cooldown.stop()

#冷却
func _on_cooldown_timeout():
	#这里潜藏一个坑：开了计时器后即使没有目标可能也会运行
	if not target_list.is_empty():
		var new_bullet:Bullet = bullet_resource.instantiate()
		new_bullet.target = target_list[0]
		new_bullet.damage = strength
		new_bullet.speed = 10 / cooldown_time #保证合理间距
		target_list[0].add_child(new_bullet)
		new_bullet.global_position = global_position #托付以后再改变位置吧
		
#		new_bullet.deal_damage.connect(target_list[0].take_damage(strength))
