extends Node3D
# 到达目标位置，等待动画播放完毕，对目标造成伤害
#如果目标提前死亡，不要报错
#——因此造成伤害应该使用信号系统
## 注意：bullet应当是TargetBody的子节点！（跟踪弹）
# 虽说如此便可以直接position而不用global，简化了，但还是做一个预备吧
#此外还有个top level来着
class_name Bullet

var target:TargetBody
var damage:float = 10
var speed:float = 20

#signal deal_damage(damage:float)

func _ready():
	#top_level = true #既然决定使用global，那便没必要toplevel了吧
	#deal_damage.connect(target.take_damage)
	pass

func _process(delta):
	if remaining_distance() > speed*delta:
		global_position = global_position + forward_direction()*speed*delta
	else:
		#deal_damage.emit(damage)
		target.take_damage(damage) #所以最后还是不用信号了？……目标消失自己也消失，正常
		queue_free()

func forward_direction():
	return (target.global_position - global_position).normalized()

func remaining_distance()->float:
	return (target.global_position-global_position).length()
