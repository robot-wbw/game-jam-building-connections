extends Area3D
class_name TargetBody
#有生命值，接受伤害
#注意：父节点为本体，此节点相当于给父节点提供功能
#在生命值归零时通知父节点
#@onready var health_bar:Label3D = $HealthBar

@export var health:float = 100:
	set(x):
		health=x
		update_health_bar()
@export var max_health:float = 100:
	set(x):
		max_health=x
		update_health_bar()

#队伍……
@export var team:int =1
var alive:bool = true #保证只一次触发，设立此

signal show_health(the_text:String)
signal die()

func take_damage(amount:float):
	health -= amount
	if health <= 0:
		go_die()

func heal(amount:float):
	health = clamp(health+amount,0,max_health)
	#满血量暂时不做操作

func update_health_bar():
	#这次就暂时不做颜色了
	show_health.emit(str(health)+" / " +str(max_health))
	
func _ready(): 
	update_health_bar()

func go_die():
	if alive:
		alive = false #防止二次信号发送
		die.emit()
		
