extends CharacterBody3D

#参考ADIL SHAFIQ在3d platformer starter的部分代码
## 除了移动的行为外，还有与地图交互的功能

@onready var world_map:WorldMap = %WorldMap

@onready var sight = $Sight
@onready var camera = $Sight/Camera
@onready var indicator:Node3D = $Indicator
var looking_at:Vector3i #存储自己视点在world_map中的坐标位置
var indicate_at:Vector3i
var indicating:bool = false #控制，一定要indicate_at找到位置以后才可建造
var indicate_what:WorldMap.ID = WorldMap.ID.NULL #正在看的方块类型
var build_what:WorldMap.ID = WorldMap.ID.NULL



@export var mouse_sensitivity := 0.2
@export var joypad_sensitivity := 200
@export var move_speed : float = 10

## 发送信号，检查将会建造什么
## 日后有滚轮了，还可以存一个当前选择的
## 其实目前只不过是看见位置，自动选择适合的建筑吧？
signal how_to_build(id:WorldMap.ID) 

## 执行建造，发送给资源管理器检查是否通过
signal build(place:Vector3i,id:WorldMap.ID) 
#signal switch_item #现在的设计只用显示一格


## 在已有建筑基础上可以建造什么
## 注意使用时先检查字典里是否有这个键key
var upgrade:Dictionary ={ #这里是反过来，于是可以直接根据块来用，而不是看“是否可替代”了
	GridMap.INVALID_CELL_ITEM : WorldMap.ID.CONNECTOR,
	WorldMap.ID.LEAF_RESOURCE : WorldMap.ID.LEAF_COLLECTOR,
	WorldMap.ID.WATER_RESOURCE : WorldMap.ID.WATER_COLLECTOR,
	WorldMap.ID.SEED_RESOURCE : WorldMap.ID.SEED_COLLECTOR,
	WorldMap.ID.CONNECTOR : WorldMap.ID.GUARD_TOWER,
	#新设计下，armytower可以作为guardtower的升级。这样只要一格显示就够了
	#WorldMap.ID.GUARD_TOWER : WorldMap.ID.ARMY_TOWER,  #兵力塔的兵力寻路暂时完成不了了……
	}


func _ready():
	top_level = true
	# Confining Mouse Cursor in the game view so it doesnt get in the way of gameplay
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _process(delta):
	get_input(delta)
	indicate()

# Handling sight Movement
func _unhandled_input(event):
	if event is InputEventMouseMotion:
		sight.rotation_degrees.x -= event.relative.y * mouse_sensitivity
		sight.rotation_degrees.x = clamp(sight.rotation_degrees.x, -90, 40)
		
		sight.rotation_degrees.y -= event.relative.x * mouse_sensitivity
		sight.rotation_degrees.y = wrapf(sight.rotation_degrees.y, 0, 360)
	
	if event.is_action_pressed("build") and indicating:
		build.emit(indicate_at,build_what)
		refresh_indecator()
		
	#if event.is_action("switch_item"): #现在的设计只用显示一格
		#switch_item.emit()
	

func get_input(delta):
	# 移动
	var move_direction := Vector3.ZERO
	move_direction.x = Input.get_axis("move_left", "move_right")
	move_direction.z = Input.get_axis("move_forward", "move_back")
	
	# Move The player Towards Spring Arm/sight Rotation
	move_direction = move_direction.rotated(Vector3.UP, sight.rotation.y)
	var elevating:float = Input.get_axis("move_down","move_up")
	velocity = Vector3(move_direction.x, elevating, move_direction.z )
	velocity = velocity * clamp(velocity.length(),0,1) * move_speed #速度设置并限速
	move_and_slide()
	
	# 视角改变，但是不是鼠标
	var sight_change := Vector2.ZERO
	sight_change.x = Input.get_axis("look_left", "look_right")
	sight_change.y = Input.get_axis("look_up", "look_down")
	
	# Move The player Towards Spring Arm/sight Rotation
	sight.rotation_degrees.x -= sight_change.y * joypad_sensitivity * delta
	sight.rotation_degrees.x = clamp(sight.rotation_degrees.x, -90, 40)
	sight.rotation_degrees.y -= sight_change.x * joypad_sensitivity * delta
	sight.rotation_degrees.y = wrapf(sight.rotation_degrees.y, 0, 360)

## 放置指示物
## 希望的逻辑是：如果下面是地面，就可以放置（即使有高低差）
## looking_at是不管下面是否地面的，但是进一步build_at就要考虑了
## indecator是以indicate_at为准的
## 第一次indicate_at生效以前，不允许建造
##
##后面想了想，如果指示器放在已建造的位置，也可以查看信息与提供提示吧
func indicate():
	#获取视点位置
	if sight.is_colliding():
		# 注意：指示器的toplevel应当开启
		looking_at = world_map.local_to_map(sight.get_collision_point()+0.1*sight.get_collision_normal())
		if world_map.get_cell_item(looking_at+Vector3i(0,-1,0)) == WorldMap.ID.GROUND:
			indicate_at = looking_at
			refresh_indecator()
		#print(sight.get_collision_point())
		# 目前离开太远也要显示吧？……或者说如果没有接触，那就是之前的位置
func refresh_indecator():
	indicating = true
	indicator.show()
	indicator.position = world_map.map_to_local(indicate_at)
	indicate_what = (world_map.get_cell_item(indicate_at)) 
	if indicate_what in upgrade.keys():
		build_what = upgrade[indicate_what]
	else :
		build_what = WorldMap.ID.NULL
	how_to_build.emit(build_what)
		
