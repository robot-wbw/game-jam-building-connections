extends Node
##游戏主循环
##
## 游戏进程：[br]
## 初始化：在对应单元格生成对应节点[br]
## 胜利条件：打败所有敌方塔（或者存活一段时间？）[br]
## 失败条件：自己主塔被摧毁[br]
##[br]
## 敌方塔不需要连接也可以存活，而己方塔必须要连接才能存活[br]
## 敌方塔生命值不回复，可以被己方兵力攻击[br]
## 己方塔：连接中的会逐渐回复生命值，不在连接中的会逐渐扣除生命值[br]
##[br]
## 场上有资源点，在资源点上建塔后可以隔一段时间产出一定的资源[br]
## 三种资源：水water 叶绿leaf（或者营养素） 种子seed[br]
## 水与叶绿产出较快，种子产出较慢[br]
## 路线只需要水和叶绿，塔则需要包括种子共三种资源[br]
## 资源点只能种资源采集塔且必须与路线连接，路线可以铺空地，防御塔只能种在线路上（或者费用自动运算？）[br]
## 拆除塔不返还资源或返还一半（目前也不能拆塔，只有靠兵力扣血）[br]
##[br]
## 关卡一般是自己有个种子塔、叶绿资源塔、水资源塔，几个资源点，敌方一堆兵力塔[br]
## 防御塔攻击范围比兵力高但射速也有限[br]
## （敌方兵力塔后期或许可以逐渐随机生成？——那么只要存活几波就胜利了）[br]

## main既是资源管理员，也是游戏进程管理员吧？
## 好吧其实大部分的工作已经让子节点完成了（点名ResourceManager）

# Called when the node enters the scene tree for the first time.
func _ready():
	#$ui.update_water(10000000000)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
	#pass



#在地图上找符合要求的塔的位置，应该是通过信号调用？或者直接使用函数return？


#连接检查，从主树开始往外拓展（通过信号？并且确实要地图为中介）
#一般来讲……先在地图操作，然后找到对应位置的节点进行操作即可
#使用信号的话，把需求给到地图，然后返回得到需要的？
#毕竟地图内容是可能改变的……我们需要的是坐标
#所以可以用await

## 处理未被连接的单元
## 通过gridmap返回的信号调取
#func deal_with_unconnected_unit(connected_units:Array[Vector3i]):
	#pass
