extends Node

func _unhandled_input(event):
	if event.is_action_pressed("full_screen"):
		var window:Window = get_window()
		match window.mode:
			Window.Mode.MODE_FULLSCREEN:
				window.mode = Window.MODE_WINDOWED
			_:
				window.mode = Window.MODE_FULLSCREEN
		
