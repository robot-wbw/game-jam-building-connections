extends Node

@export var next_level:String = ""
@export var level_title:String ="第一关 突围\n Level 1 - Escape"

@onready var delay_timer:Timer = $LevelChangeDelay
@onready var world_map:WorldMap = %WorldMap
@onready var ui = %UI
signal show_message(the_text:String)

func _ready():
	show_message.emit(level_title)
	world_map.win.connect(_on_world_map_win)


func change_level():
	if next_level == "":
		get_tree().change_scene_to_file("res://主菜单/menu.tscn")
	else:
		get_tree().change_scene_to_file(next_level)


func _on_world_map_win():
	show_message.emit("威胁已全部清除，前往下一场景……\nStage cleared! Going to next level...")
	delay_timer.start()
