extends GridMap
class_name WorldMap
## 管理块与外界实体节点的交互

## 新架构下，map也直接管理功能块的实体节点
## 这种情况下，做地图时[b]不[/b]另外放置“功能块的实体节点”
## 在游戏中，先要根据地图元素生成对应的Unit节点

##新的架构下不再使用字典记录Unit,而是以块定身份

#var units:Array[Unit] #直接引用功能节点
#好吧没用上

#signal lose
signal win

var block_to_unit:Dictionary #用于根据位置快速查找节点——所在位置的块如果改了，那就自然把值也换了
var starting_connectables:Array[Vector3i] #连线起点

enum ID{ ## 方块的数字id枚举
	GROUND,
	CONNECTOR,
	LEAF_RESOURCE,
	WATER_RESOURCE,
	SEED_RESOURCE,
	LEAF_COLLECTOR,
	WATER_COLLECTOR,
	SEED_COLLECTOR,
	GUARD_TOWER,
	ENEMY_GUARD_TOWER, #
	ARMY_TOWER, #换掉MeshLibrary后这两个顺序也换了……不过还好只要改改枚举就能继续用
	#算了……看来换掉的太多了
	ENEMY_ARMY_TOWER,
	NULL=-1
}

const CONNECTABLES:Array[ID]=[
	ID.CONNECTOR,
	ID.LEAF_COLLECTOR,
	ID.WATER_COLLECTOR,
	ID.SEED_COLLECTOR,
	ID.GUARD_TOWER,
	ID.ARMY_TOWER,
]

const COLLECTOR_TOWERS:Array[ID]=[
	ID.LEAF_COLLECTOR,
	ID.WATER_COLLECTOR,
	ID.SEED_COLLECTOR,
]

const ALLOWED_CONNECTIONS:Array[Vector3i] = [
	Vector3i(1,0,0),
	Vector3i(-1,0,0),
	Vector3i(0,0,1),
	Vector3i(0,0,-1),
	Vector3i(1,1,0),
	Vector3i(-1,1,0),
	Vector3i(0,1,1),
	Vector3i(0,1,-1),
	Vector3i(1,-1,0),
	Vector3i(-1,-1,0),
	Vector3i(0,-1,1),
	Vector3i(0,-1,-1),
]

var CLASS_LIST:Dictionary = {
	#WorldMap.ID.GROUND : null, #没有类的就别写，以免出错
	WorldMap.ID.CONNECTOR : Connector,
	WorldMap.ID.LEAF_RESOURCE : LeafResource,
	WorldMap.ID.WATER_RESOURCE : WaterResource,
	WorldMap.ID.SEED_RESOURCE : SeedResource,
	WorldMap.ID.LEAF_COLLECTOR : LeafCollector,
	WorldMap.ID.WATER_COLLECTOR : WaterCollector,
	WorldMap.ID.SEED_COLLECTOR : SeedCollector,
	WorldMap.ID.GUARD_TOWER : GuardTower,
	WorldMap.ID.ENEMY_GUARD_TOWER : GuardTower,
	WorldMap.ID.ARMY_TOWER : ArmyTower,
	WorldMap.ID.ENEMY_ARMY_TOWER : ArmyTower,
}

const LEFTOVERS:Dictionary = { #本质上是Upgrade表反过来用?但兵塔不会退化为防御塔或者线
	WorldMap.ID.CONNECTOR:GridMap.INVALID_CELL_ITEM,
	WorldMap.ID.LEAF_COLLECTOR : WorldMap.ID.LEAF_RESOURCE,
	WorldMap.ID.WATER_COLLECTOR : WorldMap.ID.WATER_RESOURCE,
	WorldMap.ID.SEED_COLLECTOR : WorldMap.ID.SEED_RESOURCE ,
	WorldMap.ID.GUARD_TOWER:GridMap.INVALID_CELL_ITEM,
	#新设计下，armytower可以作为guardtower的升级。这样只要一格显示就够了
	WorldMap.ID.ARMY_TOWER:GridMap.INVALID_CELL_ITEM, 
	WorldMap.ID.ENEMY_GUARD_TOWER:GridMap.INVALID_CELL_ITEM,
	WorldMap.ID.ENEMY_ARMY_TOWER:GridMap.INVALID_CELL_ITEM, 
	#GridMap.INVALID_CELL_ITEM:GridMap.INVALID_CELL_ITEM,#？？空气中的敌人吗（不知为何一定要这条代码才不报错）
}


func _ready():
	world_init()
	

## key是方块id（int值，根据枚举）
## value是unit实体列表（Array[对应的Unit类型]）
## 使用时，对应类型的数字代码为键，可以得到相应的unit数组（不以类名为索引因为有敌方……干脆用方块id了）

## 根据块生成对应节点
func world_init():
	#init_units(ID.CONNECTOR)
	#init_units(ID.LEAF_RESOURCE)
	#init_units(ID.WATER_RESOURCE)
	#init_units(ID.SEED_RESOURCE)
	#init_units(ID.LEAF_COLLECTOR)
	#init_units(ID.WATER_COLLECTOR)
	#init_units(ID.SEED_COLLECTOR)
	#init_units(ID.GUARD_TOWER)
	#init_units(ID.ARMY_TOWER)
	#init_units(ID.LEAF_COLLECTOR)
	#init_units(ID.ENEMY_GUARD_TOWER)
	#init_units(ID.ENEMY_ARMY_TOWER)
	#
	#下面这个试图极致简化，但是enum如何出数字类型呢
	#好吧values()可以
	#print(ID.values())
	for id in ID.values():
		for location in get_used_cells_by_item(id):
			set_unit(location,id)
	
	#修改：现在只有一开始在地图出现 的种子塔是独立的，其他都需要连接才可活跃
	for unit in get_children():
		if unit is SeedCollector:
			unit.independent = true 
			starting_connectables.append(unit.map_position)
	check_connection()
	
#可行，真的可行
## 对于每一种类型的块，使用一种类型的Unit来初始化，并且允许函数操作新的Unit修改属性
func init_units(id:ID):
	for location in get_used_cells_by_item(id):
		set_unit(location,id)

func set_unit(location:Vector3i,id:ID):
	if CLASS_LIST.has(id):
		var new:Unit = CLASS_LIST[id].new()
		new.position = map_to_local(location)
		if id == ID.ENEMY_GUARD_TOWER or id == ID.ENEMY_ARMY_TOWER:
			if new is WarTower:
				new.team=2
				new.is_activated =true
		add_child(new)
		block_to_unit[location] = new
	else:
		block_to_unit.erase(location)


## 可能被其他操作调用，也可以单独使用
## 沿用mc命令的名称哈哈哈哈哈哈哈哈哈咳咳
func setblock(place:Vector3i,id:ID=ID.NULL):
	#加一个判断，确定一下是否执行大规模检查
	var need_check:bool = should_i_check_it(place,id,get_cell_item(place))
	# 不管前任如何，直接更改
	set_cell_item(place,id)
	var predecessor = unit_at(place)
	if predecessor != null:
		predecessor.queue_free()
	#加新的节点
	set_unit(place,id)
	if need_check:
		check_connection()

func should_i_check_it(place:Vector3i,new_id:ID,old_id:ID)->bool:
	var need_check:bool=false
	if CONNECTABLES.has(new_id) or CONNECTABLES.has(old_id): #异或操作，表示有变化……然而防御塔和兵力塔放置时会重置
		for check_point in ALLOWED_CONNECTIONS:
			if CONNECTABLES.has(get_cell_item(check_point+place)): #对周边是否有影响
				need_check=true
	return need_check

## 返回对应位置的Unit
func unit_at(target_location:Vector3i)->Unit:
	#for unit in get_children(): 
		#if unit.map_position == target_location :
			#return unit
	##如果找不到，返回空值
	#return null
	#
	if block_to_unit.has(target_location):
		return block_to_unit[target_location]
	else:
		return null

func units_at(target_locations:Array[Vector3i])->Array[Unit]:
	#for unit in get_children(): 
		#if unit.map_position == target_location :
			#return unit
	##如果找不到，返回空值
	#return null
	#
	var result:Array[Unit] =[]
	for location in target_locations:
		if block_to_unit.has(location):
			result.append(block_to_unit[location])
	return result

#根本没用上
### 根据类型查找
### 因为字典是以类型为键，所以这样可以更高效地找unit
#func typed_unit_at(target_location:Vector3i,type:ID)->Unit:
	#if type == get_cell_item(target_location):
		#for unit in get_children():
			#if unit.map_position == target_location:
				#return unit
	##如果找不到，返回空值
	#return null
	

# 目前太麻烦，还是直接，建造更新血量吧
#func build(place:Vector3i, id:ID=ID.NULL):
	# 需要管前任，继承生命值、队伍等
	#pass

## 加上某种限制条件，对号入座了
func get_units(limitation:Callable = func(_x:Unit)->bool: return true)->Array[Unit]:
	var units:Array[Unit] = []
	for unit in get_children():
		if unit is Unit:
			if limitation.call(unit):
				units.append(unit)
	#如果找不到，返回空值
	return units

func count_units(limitation:Callable = func(_x:Unit)->bool: return true)->int:
	var count:int = 0
	for unit in get_children():
		if unit is Unit:
			if limitation.call(unit):
				count += 1
	#如果找不到，返回空值
	return count

## 相比计数Unit，更加简洁高效的计数方法
## 而单种类计数直接用get_used_cells_by_item(id).size()
## 按种类找块则是 get_used_cells_by_item(id)
func count_blocks(id_list:Array[ID])->int: 
	# 可惜这样不能判断是否失活——而且失活不能只是数据，还要表现出来，因此非Unit不可……
	# 但确实可以先通过这个来定个位？然而本身Unit“定位”也只是依次比较吧……不如直接看是否active？
	# 不过就可以，以位置代替class类型的搜索条件了吧？
	var count:int = 0
	for id in id_list:
		count += get_used_cells_by_item(id).size()
	return count
	
	
#没用的东西
#func count_units_in(places:Array[Vector3i],limitation:Callable = func(_x:Unit)->bool: return true)->int:
	#var count:int = 0
	#for unit in get_children():
		#if unit is Unit:
			#if places.has(unit.map_position) and limitation.call(unit):
				#count += 1
	##如果找不到，返回空值
	#return count

#参考函数，不过一般还是需要时再写的好……单独为一个条件列一个函数有点太奇怪
#func is_independent_connectable(unit:Unit)->bool:
	#if unit is Connectable:
		#if unit.independent == true:
			#return true
	#return false
	#
#func in_team(unit:Unit,team:int)->bool:
	#if unit is Building:
		#if unit.team == team:
			#return true
	#return false

## 由main通过信号调用，返回给main一个结果
## 返回结果为正常连接的单元格，由main操作未连接的单元格
## 地图更新时调用（更新：setblock（包括了玩家建造，因为经过setblock……后面防御塔被摧毁应该也是setblock））
## 此外，地图初始化以后也要检查一次
func check_connection():
	# 这个基本上逃不了的，如果要进行统计筛选的话，确实只有这边统一操作
	# 失活、重新活跃等操作，都可以外面这里来——连上的就活跃，剩下的的就失活
	
	# 一堆算法……从主树开始往外拓展
	# 然后把相邻的逐渐加起来（用过的不再用，用最新一圈，找不在用过的数组里的作为新一圈）
	#var is_starting:Callable = func(unit:Unit)->bool:
		#if unit is Connector:
			#return unit.independent and unit.team == 1
		#return false
	#var staring = get_units(is_starting)
	
	# 既然地图与Unit是一一绑定的，那么直接在地图里找岂不是更快？……不过需要穷举？
	#确定范围
	var connectables:Array[Vector3i] =[]
	for id in CONNECTABLES:
		connectables.append_array(get_used_cells_by_item(id))
	
	# 初始化
	var activated_connectables:Array[Vector3i] = []	#记录连接库
	var new_starts:Array[Vector3i] = []	#探索的起点
	var new_founds:Array[Vector3i] = []	#探索点
	
	new_starts.assign(starting_connectables)
	#循环
	while(not new_starts.is_empty()):
		activated_connectables.append_array(new_starts)
		for new_start in new_starts:
			var finding_points:Array[Vector3i] = []
			for i in ALLOWED_CONNECTIONS.size():
				finding_points.append(ALLOWED_CONNECTIONS[i] + new_start)
			for new_found in finding_points:
				#实际上是作交集，不过好像没有内置函数
				if connectables.has(new_found) and not activated_connectables.has(new_found):
					new_founds.append(new_found)
		new_starts.assign(new_founds)
		new_founds.clear()
	
	# 补集
	var inactivated_connectables:Array[Vector3i] = []
	for connectable in connectables:
		if not activated_connectables.has(connectable):
			inactivated_connectables.append(connectable)
			
	#block_to_unit[
	#执行：“是否活跃”的操作
	#var activating_unit:Array[Unit] = get_units(func(unit:Unit)->bool: return activated_connectables.has(unit.map_position))
	#var inactivating_unit:Array[Unit] = get_units(func(unit:Unit)->bool: return inactivated_connectables.has(unit.map_position))
	#连接检查经常使用，这一步怎么可能交给基于for的查找
	var activating_units:Array[Unit] = units_at(activated_connectables)
	var inactivating_units:Array[Unit] = units_at(inactivated_connectables)
	
	for unit in activating_units:
		if unit is Connectable and unit.is_activated == false:
			unit.is_activated = true
	for unit in inactivating_units:
		if unit is Connectable and unit.is_activated == true:
			unit.is_activated = false
	
func _on_building_die(place:Vector3i):
	setblock(place,LEFTOVERS[get_cell_item(place)])
	check_win_or_lose()

func check_win_or_lose():
	#TODO: ……或许还是getunit的好？
	#TODO: 为啥自己的兵力塔会 攻击，而且阵营还不同？
	print(get_used_cells_by_item(ID.ENEMY_GUARD_TOWER))
	print(get_used_cells_by_item(ID.ENEMY_ARMY_TOWER))
	if get_used_cells_by_item(ID.ENEMY_GUARD_TOWER).is_empty() and get_used_cells_by_item(ID.ENEMY_ARMY_TOWER).is_empty():
		#win
		win.emit()
	#失败不设自动重来，但是有个手动重来
	#因为失败的判定……需要资源用尽，而且塔也没有了才行……比较麻烦
		
