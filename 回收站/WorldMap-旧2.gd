extends GridMap
class_name WorldMap
## 管理块与外界实体节点的交互

## 新架构下，map也直接管理功能块的实体节点
## 这种情况下，做地图时[b]不[/b]另外放置“功能块的实体节点”
## 在游戏中，先要根据地图元素生成对应的Unit节点

var units:Array[Unit] #直接引用功能节点

enum ID{ ## 方块的数字id枚举
	GROUND,
	CONNECTOR,
	LEAF_RESOURCE,
	WATER_RESOURCE,
	SEED_RESOURCE,
	LEAF_COLLECTOR,
	WATER_COLLECTOR,
	SEED_COLLECTOR,
	GUARD_TOWER,
	ENEMY_GUARD_TOWER,
	ARMY_TOWER,
	ENEMY_ARMY_TOWER,
	NULL=-1
}
const COLLECTOR_TOWERS:Array[int]=[
	ID.LEAF_COLLECTOR,
	ID.WATER_COLLECTOR,
	ID.SEED_COLLECTOR,
]
var CLASS_LIST:Dictionary = {
	WorldMap.ID.GROUND : null,
	WorldMap.ID.CONNECTOR : Connector,
	WorldMap.ID.LEAF_RESOURCE : LeafResource,
	WorldMap.ID.WATER_RESOURCE : WaterResource,
	WorldMap.ID.SEED_RESOURCE : SeedResource,
	WorldMap.ID.LEAF_COLLECTOR : LeafCollector,
	WorldMap.ID.WATER_COLLECTOR : WaterCollector,
	WorldMap.ID.SEED_COLLECTOR : SeedCollector,
	WorldMap.ID.GUARD_TOWER : GuardTower,
	WorldMap.ID.ENEMY_GUARD_TOWER : GuardTower,
	WorldMap.ID.ARMY_TOWER : ArmyTower,
	WorldMap.ID.ENEMY_ARMY_TOWER : ArmyTower,
}



func _ready():
	world_init()
	

## key是方块id（int值，根据枚举）
## value是unit实体列表（Array[对应的Unit类型]）
## 使用时，对应类型的数字代码为键，可以得到相应的unit数组（不以类名为索引因为有敌方……干脆用方块id了）
var unit_list:Dictionary #统一操作，而且按方块id定位
var unit_count:Dictionary ## 每种类型计数（key用数字id）
var active_count:Dictionary ## 目前不考虑地图上自带失活塔吧

## 根据块生成对应节点
func world_init():
	unit_list[ID.CONNECTOR]=init_units(ID.CONNECTOR,Connector)
	unit_list[ID.LEAF_RESOURCE]=init_units(ID.LEAF_RESOURCE,LeafResource)
	unit_list[ID.WATER_RESOURCE]=init_units(ID.WATER_RESOURCE,WaterResource)
	unit_list[ID.SEED_RESOURCE]=init_units(ID.SEED_RESOURCE,SeedResource)
	unit_list[ID.LEAF_COLLECTOR]=init_units(ID.LEAF_COLLECTOR,LeafCollector)
	unit_list[ID.WATER_COLLECTOR]=init_units(ID.WATER_COLLECTOR,WaterCollector)
	unit_list[ID.SEED_COLLECTOR]=init_units(ID.SEED_COLLECTOR,SeedCollector)
	unit_list[ID.GUARD_TOWER]=init_units(ID.GUARD_TOWER,GuardTower)
	unit_list[ID.ARMY_TOWER]=init_units(ID.ARMY_TOWER,ArmyTower)
	unit_list[ID.LEAF_COLLECTOR]=init_units(ID.LEAF_COLLECTOR,LeafCollector)
	unit_list[ID.ENEMY_GUARD_TOWER]=init_units(ID.ENEMY_GUARD_TOWER,GuardTower,func(x:GuardTower)-> GuardTower: x.team=2;return x)
	unit_list[ID.ENEMY_ARMY_TOWER]=init_units(ID.ENEMY_ARMY_TOWER,ArmyTower,func(x:ArmyTower)-> ArmyTower: x.team=2;return x)
	
	#计数、挂在树上
	for key in unit_list: 
		unit_count[key]=unit_list[key].size()
		if key in COLLECTOR_TOWERS:
			unit_count[key]=unit_list[key].size() #初始资源塔计数
		for unit in unit_list[key]:
			add_child(unit)
	
#可行，真的可行
## 对于每一种类型的块，使用一种类型的Unit来初始化，并且允许函数操作新的Unit修改属性
func init_units(ID_id:ID,unit_class:Resource,operation:Callable = func(x:Unit)->Unit:return x)->Array[Unit]:
	var result:Array[Unit] = []
	for location in get_used_cells_by_item(ID_id):
		var new = unit_class.new()
		new.position = map_to_local(location)
		new=operation.call(new)
		result.append(new)
	return result

## 返回对应位置的Unit
func unit_at(target_location:Vector3i)->Unit:
	for key in unit_list: 
		for unit in unit_list[key]:
			if unit.map_position == target_location :
				return unit
	#如果找不到，返回空值
	return null

func remove_unit(unit:Unit):
	# 存了类型，这里就找类型了——反而是节点树下不能直接返回“类型”
	# 好像也是这里唯一反过来可以找到Unit类型的方法了
	var type:ID = unit_list.find_key(unit) #然而mull=0
	if type != null:
		unit_count[type] -= 1
	unit.queue_free()

func add_unit(unit:Unit):
	pass

## 根据类型查找
## 因为字典是以类型为键，所以这样可以更高效地找unit
func typed_unit_at(target_location:Vector3i,type:ID)->Unit:
	for unit in unit_list[type]:
		if unit.map_position == target_location :
			return unit
	#如果找不到，返回空值
	return null
	


## 由main通过信号调用，返回给main一个结果
## 返回结果为正常连接的单元格，由main操作未连接的单元格
func check_connection():
	# 这个基本上逃不了的，如果要进行统计筛选的话，确实只有这边统一操作
	# 失活、重新活跃等操作，都可以外面这里来——连上的就活跃，剩下的的就失活
	
	# 一堆算法……从主树开始往外拓展
	# 然后把相邻的逐渐加起来（用过的不再用，用最新一圈，找不在用过的数组里的作为新一圈）
	#
	#
	#
	pass


## 可能被其他操作调用，也可以单独使用
## 沿用mc命令的名称哈哈哈哈哈哈哈哈哈咳咳
func setblock(place:Vector3i,id:ID=ID.NULL):
	# 不管前任如何，直接更改
	unit_at(place).queue_free()
	set_cell_item(place,id)
	#TODO: 加新的节点

#func count_typed_units(type:Resource):
	#var count = 0
	#for unit in get_children():
		#count += 1
	#pass
	

# 目前太麻烦，还是直接，建造更新血量吧
#func build(place:Vector3i, id:ID=ID.NULL):
	## 需要管前任，继承生命值、队伍等
	#pass
